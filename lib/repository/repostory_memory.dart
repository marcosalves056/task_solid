import 'package:atividade_1/model/task_model.dart';
import 'package:atividade_1/repository/repostiory_interface.dart';

class TaskRepositoryMemory
    implements
        CreateInterface,
        RemoveInterface,
        UpdateInterface,
        ListInterface,
        CheckTask,
        Initial {
  late List<TaskModel> _list;

  TaskRepositoryMemory() {
    initial();
  }

  @override
  Future<void> create(TaskModel task) async {
    list().add(task);
  }

  @override
  Future<void> remove(TaskModel task) async {
    list().removeWhere((e) => e == task);
  }

  @override
  List<TaskModel> list() {
    return _list;
  }

  @override
  Future<void> update(TaskModel task) async {
    list()[list().indexWhere((e) => e == task)] = task;
  }

  @override
  Future<void> initial() async {
    _list = <TaskModel>[];
  }

  @override
  void check(TaskModel task, bool check) {}
}
