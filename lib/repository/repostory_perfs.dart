import 'dart:convert';

import 'package:atividade_1/repository/repostiory_interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:atividade_1/model/task_model.dart';

class TaskRepositorySharedPreferences
    implements
        CreateInterface,
        RemoveInterface,
        UpdateInterface,
        ListInterface,
        CheckTask,
        Initial {
  List<TaskModel>? _list = [];

  @override
  Future<void> create(TaskModel task) async {
    _list!.add(task);
    await save();
  }

  @override
  Future<void> remove(TaskModel task) async {
    _list!.removeWhere((e) => e == task);
    await save();
  }

  @override
  List<TaskModel> list() {
    return _list!;
  }

  @override
  Future<void> update(TaskModel task) async {
    _list![_list!.indexWhere((e) => e == task)] = task;
    await save();
  }

  Future<void> save() async {
    var prefs = await SharedPreferences.getInstance();
    var listString = _list!.map((e) => e.toJson()).toList().toString();
    await prefs.setString('mytasks', listString);
  }

  @override
  Future<void> initial() async {
    var prefs = await SharedPreferences.getInstance();
    var listString = prefs.getString('mytasks');

    if (listString == null) {
      _list = [];
    } else {
      _list = [];

      List array = jsonDecode(listString);

      for (var element in array) {
        _list!.add(
          TaskModel.fromMap(element),
        );
      }
    }
  }

  @override
  void check(TaskModel task, bool check) {}
}


// import 'dart:convert';

// import 'package:atividade_1/model/task_model.dart';
// import 'package:atividade_1/repository/repostiory_interface.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// class RepositoryPrefs
//     implements CreateInterface, RemoveInterface, ListInterface, CheckTask {
//   final tasks = <TaskModel>[];

//   @override
//   void create(TaskModel taskModel) async {
//     SharedPreferences shared = await SharedPreferences.getInstance();

//     var taskString = jsonEncode({
//       'id': taskModel.id,
//       'description': taskModel.description,
//       'checked': taskModel.checked
//     });

//     shared.setStringList('tasks', [taskString]);
//   }

//   @override
//   void remove(TaskModel taskModel) {
//     // TODO: implement remove
//   }

//   @override
//   List<TaskModel> list() {
//     var list = shared.getStringList('tasks');
//     var lista = <TaskModel>[];
//     var task = "";
//     if (list != null) {
//       for (var taski in list) {
//         task = jsonDecode(taski);
//         var checked = task[2].toLowerCase() == "true" ? true : false;
//         lista.add(
//             TaskModel(id: task[0], description: task[1], checked: checked));
//       }
//       lista.map((e) => print(e.id));
//       return lista;
//     }
//     lista = [];
//     return lista;
//   }

//   @override
//   void check(TaskModel task, bool check) {
//     // TODO: implement check
//   }

//   void initial() async {
//     SharedPreferences shared = await SharedPreferences.getInstance();
//     // return shared;
//   }
// }


