class TaskModel {
  String id;
  String description;
  bool checked;

  TaskModel({
    required this.id,
    required this.description,
    this.checked = false,
  });

  static TaskModel fromMap(element) {
    return element;
  }

  toJson() {}
}
