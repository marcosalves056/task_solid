import 'package:atividade_1/model/task_model.dart';
import 'package:atividade_1/repository/repostory_perfs.dart';
import 'package:flutter/material.dart';

class TaskView extends StatefulWidget {
  const TaskView({Key? key}) : super(key: key);

  @override
  _TaskViewState createState() => _TaskViewState();
}

class _TaskViewState extends State<TaskView> {
  final _repository = TaskRepositorySharedPreferences();
  //final _taskViewModel = TaskRepositoryMemory();
  final _editController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Taks')),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            TextField(
              decoration: const InputDecoration(label: Text('New Task')),
              controller: _editController,
              onSubmitted: (value) {
                setState(() {
                  _repository.create(TaskModel(
                      id: DateTime.now().toString(), description: value));
                  _editController.clear();
                });
              },
            ),
            const SizedBox(height: 10),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: _repository.list().length,
                itemBuilder: (_, index) {
                  var item = _repository.list();
                  var todo = item[index].description;
                  return GestureDetector(
                    onLongPress: () {
                      setState(() {
                        _repository.remove(item[index]);
                      });
                    },
                    onTap: () {
                      setState(() {
                        _buildAlertDialog(item[index], context, _repository);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ListTile(
                        title: Text(todo),
                      ),
                    ),
                  );
                  //return const SizedBox(height: 10);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_buildAlertDialog(TaskModel task, BuildContext context,
    TaskRepositorySharedPreferences _repository) {
  final dialogController = TextEditingController();
  dialogController.text = task.description;
  showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            insetPadding: const EdgeInsets.all(20),
            title: const Text('Editar Task'),
            content: Column(
              children: [
                TextField(
                  controller: dialogController,
                ),
              ],
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    task.description = dialogController.text;
                    _repository.update(task);
                    dialogController.clear();
                    Navigator.pop(context, 'ok');
                  },
                  child: const Text('save'))
            ],
          ));
}
